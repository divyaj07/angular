import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatStepperModule } from '@angular/material/stepper';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatTableModule} from '@angular/material/table';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatRadioModule} from '@angular/material/radio';
import {MatBadgeModule} from '@angular/material/badge';
import {MatSelectModule} from '@angular/material/select';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDividerModule} from '@angular/material/divider';
import {MatCardModule} from '@angular/material/card';
import {MatToolbarModule} from '@angular/material/toolbar'; 
import {MatListModule} from '@angular/material/list';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';


const MaterialComponents = [MatStepperModule, MatTableModule,MatIconModule, MatProgressSpinnerModule, MatRadioModule,MatListModule,MatToolbarModule,MatCardModule,MatDividerModule,MatExpansionModule,MatSelectModule,MatBadgeModule,MatMenuModule, MatInputModule, MatButtonModule, MatButtonToggleModule, MatCheckboxModule,MatDatepickerModule,MatDialogModule, MatSidenavModule,MatFormFieldModule,MatTooltipModule,MatSnackBarModule]

@NgModule({
  imports: [CommonModule, MaterialComponents],
  exports: [MaterialComponents]
})
export class MaterialModule { }
