import { Component, OnInit } from '@angular/core';
import {environment} from '../environments/environment';
import { HttpClient, HttpErrorResponse, HttpBackend } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  ServerUrl = environment.baseUrl;

  detailsiteinfo

  constructor(private http: HttpClient) {}

  ngOnInit(){
    this.getSiteInfo()
  }

  getSiteInfo(){
    this.http.get(this.ServerUrl + 'api/details').subscribe((res)=>{
      this.detailsiteinfo = res
      console.log(res)
    })
  }

  title = 'dummy';


}
